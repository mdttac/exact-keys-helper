const assert = require('chai').assert;
const exactAuth = require('../exactAuth.js');

describe('exactAuth.js', () => {
    describe('#()', () => {
        it('should be a function', () => {
            assert.isFunction(exactAuth);
        });

        it('should return an object', () => {
            assert.isObject(exactAuth());
        });
    });

    describe('#code.getUri()', () => {
        it('should return a valid URI', () => {
            const auth = exactAuth();
            const expected = 'https://start.exactonline.com/api/oauth2/auth?client_id=lolclientid&redirect_uri=http%3A%2F%2F127.0.0.1%3A3000%2Fcallback&scope=&response_type=code&state=&force_login=0';
            assert.equal(auth.code.getUri(), expected);
        });
    });
});