const assert = require('chai').assert;

describe('index.js', () => {
    describe('env', () => {
        beforeEach(() => {
            process.env.TESTING = 1;
        });

        it('should load from the .env file', () => {
            let index = require('../index.js');

            assert.equal(process.env.DOTENV_LOADED, 1);
        });
    });
});