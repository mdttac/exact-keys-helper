require('dotenv').config();

const exactOauth = require('./exactAuth.js');
const express = require('express');
const pug = require('pug');
const path = require('path');

const app = express();
const exact = exactOauth();

app.set('view engine', 'pug');
app.use('/static', express.static(path.join(__dirname, 'static')));
app.use('/b', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/css')));

var port = process.env.PORT || 3000;

app.get('/', (req, res) => {
    let uri = exact.code.getUri();

    res.redirect(uri);
});

app.get('/callback', (req, res) => {
    exact.code.getToken(req.originalUrl).then((user) => {
        let stripped = {
            accessToken: user.accessToken,
            refreshToken: user.refreshToken
        };

        res.render('callback', {keys: stripped});
    })
    .catch((err) => {
        console.log(err);
    });
});

if (typeof process.env.TESTING === 'undefined') {
    server = app.listen(port);

    console.log(`Server now listening for requests on port ${port}...`);
}