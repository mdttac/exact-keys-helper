exact-keys-helper will help you to manually get an accessToken and refreshToken from Exact Online's OAuth2 API in order to make subsequent requests.  If you needed to use Postman or CURL to make API requests to Exact outside of code something like this might come in handy.

# Usage
In the project directory, install prereqs:

```
$ npm install
```

In Exact, generate your API keys and make sure to set your callback URI to `http://127.0.0.1:3000/callback`.

Output your client ID and secret to the `.env` file (you can also use env variables without issue):

```
$ echo "CLIENT_ID=whatever" >> .env
$ echo "CLIENT_SECRET=lol" >> .env
```

You can optionally set the PORT environment variable to run the server that handles callbacks on a different port (make sure to set your callback URI correctly if you do this!).

Navigate to http://127.0.0.1:3000/ to begin the flow.