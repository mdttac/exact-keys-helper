const clientOauth2 = require('client-oauth2');

module.exports = () => {
    return new clientOauth2({
        clientId: process.env.CLIENT_ID || 'lolclientid',
        clientSecret: process.env.CLIENT_SECRET || 'lolclientsecret',
        accessTokenUri: 'https://start.exactonline.com/api/oauth2/token',
        authorizationUri: 'https://start.exactonline.com/api/oauth2/auth',
        redirectUri: 'http://127.0.0.1:3000/callback',
        query: {
            force_login: 0
        }
    });
};
